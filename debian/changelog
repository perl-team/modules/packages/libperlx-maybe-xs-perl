libperlx-maybe-xs-perl (1.001-6) unstable; urgency=medium

  * Team upload.
  * Add Rules-Requires-Root: no.
  * Switch from cdbs to dh. (Closes: #1089370)
  * debian/watch: use uscan version 4 and uscan macros.
  * Add hardening flags.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Sat, 07 Dec 2024 22:14:05 +0100

libperlx-maybe-xs-perl (1.001-5) unstable; urgency=medium

  * Team upload.
  * Remove build artifacts via debian/clean. (Closes: #1047418)

 -- gregor herrmann <gregoa@debian.org>  Thu, 07 Mar 2024 18:21:22 +0100

libperlx-maybe-xs-perl (1.001-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Wrap long lines in changelog entries: 1.001-3.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 14:03:06 +0000

libperlx-maybe-xs-perl (1.001-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.
  * debian/*: update GitHub URLs to use HTTPS.

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.
  * Bump debhelper dependency to >= 8, since that's what is used in
    debian/compat.
  * Bump debhelper from deprecated 8 to 10.

  [ Jenkins ]
  * Set Testsuite header for perl package.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 00:05:28 +0100

libperlx-maybe-xs-perl (1.001-2) unstable; urgency=medium

  * Update copyright info:
    + Update coverage for CONTRIBUTING file (freeing 2k of legalese).

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 30 May 2014 18:58:28 +0200

libperlx-maybe-xs-perl (1.001-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Switch from plain ExtUtils::MakeMaker to Dist::Inkt.

  [ Jonas Smedegaard ]
  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Update copyright info:
    + Include ancient copyright holders for pport.h.
    + Add git URL as alternate source.
    + Extend coverage for main upstream author, and of packaging.
    + Cover CONTRIBUTING file, lcenses as CC-BY-SA_UK-2.0.
  * Bump to standards-version 3.9.5.
  * Fix use canonical Vcs-Git URL.
  * Update watch file to use metacpan.org URL.
  * Stop track tarball checksum: Check against upstrem git instead.
  * Update package relations:
    + Enhance libperlx-maybe-perl.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 18 May 2014 15:43:09 +0200

libperlx-maybe-xs-perl (0.005-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#705062.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Apr 2013 23:53:36 +0200
